package com.vietcombank.service.repository;

import com.vietcombank.service.entity.MovieTableEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MovieTableEntityRepository extends JpaRepository<MovieTableEntity, Long> {
}