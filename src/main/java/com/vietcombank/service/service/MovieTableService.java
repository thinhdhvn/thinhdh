package com.vietcombank.service.service;

import com.vietcombank.service.entity.MovieTableEntity;
import com.vietcombank.service.repository.MovieTableEntityRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Slf4j
@Service
public class MovieTableService {
    @Autowired
    private MovieTableEntityRepository movieTableEntityRepository;

    public List<MovieTableEntity> retrieveAll() {
        return movieTableEntityRepository.findAll();
    }

    public Optional<MovieTableEntity> retrieveById(Long movieId) {
        return movieTableEntityRepository.findById(movieId);
        //return movieTableEntityRepository.getReferenceById(movieId);
    }

   public MovieTableEntity create(MovieTableEntity movie) {
        return movieTableEntityRepository.save(movie);     
    }

    public MovieTableEntity update(MovieTableEntity movie) {
        
        Optional<MovieTableEntity> reMovie = retrieveById(movie.getMovieId());
        if (reMovie == null) 
        return null; 

        return movieTableEntityRepository.save(movie);     
    }

    public int deleteById(Long movieId) {
        
        Optional<MovieTableEntity> movie = retrieveById(movieId);
        if (movie == null) 
        return 0; 

        movieTableEntityRepository.deleteById(movieId);
        return 1;
    }

}
