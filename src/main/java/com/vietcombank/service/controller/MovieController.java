package com.vietcombank.service.controller;

import com.vietcombank.service.entity.MovieTableEntity;
import com.vietcombank.service.service.MovieTableService;
import io.swagger.v3.oas.annotations.Operation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping("/restapi")
public class MovieController {
    @Autowired
    private MovieTableService movieTableService;

    @GetMapping(value = "/v1/movies/getAll", produces = "application/json")
    @Operation(summary = "Retrieve All Movies")
    public ResponseEntity<?> retrieveAllMovies() {
        log.info("retrieve all movies");
        return ResponseEntity.ok(movieTableService.retrieveAll());        
    }

    @GetMapping(value = "/v1/movies/getById", produces = "application/json")
    @Operation(summary = "Retrieve Movie By Id")
    public ResponseEntity<?> retrieveMovie(Long movieId) {
        log.info("retrieve movie by id");
        return ResponseEntity.ok(movieTableService.retrieveById(movieId));
    }

    @GetMapping(value = "/v1/movies/create", produces = "application/json")
    @Operation(summary = "Create Movie")
    public ResponseEntity<?> createMovie(MovieTableEntity movie) {
        log.info("create movie");
        return ResponseEntity.ok(movieTableService.create(movie));
    }

    @GetMapping(value = "/v1/movies/update", produces = "application/json")
    @Operation(summary = "Update Movie")
    public ResponseEntity<?> updateMovie(MovieTableEntity movie) {
        log.info("update movie");
        return ResponseEntity.ok(movieTableService.update(movie));
    }

    @GetMapping(value = "/v1/movies/deleteById", produces = "application/json")
    @Operation(summary = "Delete Movie By Id")
    public ResponseEntity<?> DeleteMovie(Long movieId) {
        log.info("delete movie by id");
        return ResponseEntity.ok(movieTableService.deleteById(movieId));
    }
}
