create database Movie;

CREATE SEQUENCE Movie_Type_Sequence start 1 increment 1;

CREATE TABLE Movie_Type
(
    type_id         int PRIMARY KEY,
    type_name       varchar(200)
);

insert into Movie_Type(type_id, type_name) values (nextval('Movie_Type_Sequence'), 'Action');
insert into Movie_Type(type_id, type_name) values (nextval('Movie_Type_Sequence'), 'Detective');

select * from Movie_Type;

CREATE SEQUENCE Movie_Sequence start 1 increment 1;

--drop table Movie;
CREATE TABLE Movie
(
    movie_id        	int PRIMARY KEY,
    type_id       int,
    movie_name			varchar(200),
    description         text
);


insert into Movie(movie_id, type_id, movie_name, description) values (1, 1, 'Film01', 'Description about Film01');
insert into Movie(movie_id, type_id, movie_name, description) values (2, 2, 'Film02', 'Description about Film02');

select * from Movie;