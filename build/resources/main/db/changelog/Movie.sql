--liquibase formatted sql
--changeset microservice_class:20230725_1


CREATE SEQUENCE Movie_Sequence start 1 increment 1;

--drop table Movie;
CREATE TABLE Movie
(
    movie_id        	int PRIMARY KEY,
    image_link          varchar(200),
    movie_name			varchar(200),
    description         text
);


insert into Movie(movie_id, image_link, movie_name, description) values (nextval('Movie_Sequence'), 'https://movies.universalpictures.com/media/opr-tsr1sheet3-look2-rgb-3-1-1-64545c0d15f1e-1.jpg', 'OPPENHEIMER', 'Với nhân vật trung tâm là J. Robert Oppenheimer, nhà vật lý lý thuyết người đứng đầu phòng thí nghiệm Los Alamos, thời kỳ Thế chiến II. Ông đóng vai trò quan trọng trong Dự án Manhattan, tiên phong trong nhiệm vụ phát triển vũ khí hạt nhân, và được coi là một trong những cha đẻ của bom nguyên tử.');
insert into Movie(movie_id, image_link, movie_name, description) values (nextval('Movie_Sequence'), 'http://starlight.vn/Areas/Admin/Content/Fileuploads/images/POSTER/talk-to-me.jpg', 'GỌI HỒN QUỶ DỮ', 'Một nhóm bạn phát hiện ra bàn tay ma quái cho phép họ triệu hồi các linh hồn bí ẩn. Họ dần bị cuốn vào trò chơi này mà không biết rằng một trong số họ đã đi quá xa và giải phóng thế lực hắc ám kinh hoàng.');
insert into Movie(movie_id, image_link, movie_name, description) values (nextval('Movie_Sequence'), 'https://i.imgur.com/G4fLl4f.png', 'PHIM ĐIỆN ẢNH DORAEMON: NOBITA VÀ VÙNG ĐẤT LÝ TƯỞNG TRÊN BẦU TRỜI', 'Phim điện ảnh Doraemon: Nobita Và Vùng Đất Lý Tưởng Trên Bầu Trời kể câu chuyện khi Nobita tìm thấy một hòn đảo hình lưỡi liềm trên trời mây. Ở nơi đó, tất cả đều hoàn hảo… đến mức cậu nhóc Nobita mê ngủ ngày cũng có thể trở thành một thần đồng toán học, một siêu sao thể thao. Cả hội Doraemon cùng sử dụng một món bảo bối độc đáo chưa từng xuất hiện trước đây để đến với vương quốc tuyệt vời này. Cùng với những người bạn ở đây, đặc biệt là chàng robot mèo Sonya, cả hội đã có chuyến hành trình tới vương quốc trên mây tuyệt vời… cho đến khi những bí mật đằng sau vùng đất lý tưởng này được hé lộ.');
insert into Movie(movie_id, image_link, movie_name, description) values (nextval('Movie_Sequence'), 'https://cdn.moveek.com/storage/media/cache/large/64deafb47dd9b586527172.jpg', ' BLUE BEETLE', 'Cậu sinh viên mới tốt nghiệp Jaime Reyes trở về nhà với tràn trề niềm tin và hy vọng về tương lai, để rồi nhận ra quê nhà của anh đã thay đổi rất nhiều so với trước đây. Khi tìm kiếm mục đích sống trên thế giới này, Jamie đối mặt với bước ngoặt cuộc đời khi anh nhận ra mình sở hữu một di sản cổ đại của công nghệ sinh học ngoài hành tinh: Scarab (Bọ Hung). Khi Scarab chọn Jamie trở thành vật chủ, anh được ban tặng một bộ áo giáp với siêu sức mạnh đáng kinh ngạc không ai có thể lường trước. Số phận của Jamie hoàn toàn thay đổi khi giờ đây, anh đã là siêu anh hùng BLUE BEETLE.');

